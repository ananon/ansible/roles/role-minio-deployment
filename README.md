# Minio Deployment Role

This role will deploy:
- minio operator by applying minio-operator.yml
  * 1 CRD, 1 Deployment, 1 Pod, 1 Replica set
- minio tenant by applying minio-tenant.yml
  * 1 Stateful sets, 3 Pods, 6 PVC, 3 Services
- minio route by applying minio-route-https.yml 
  * 1 Route
- minio s3 bucket for further usage by quay

Role that can be run also independently from another template if needed

 - role-minio-deployment

Role includes
- main.yml under tasks folder
- minio-operator.yml under templates folder
- minio-tenant.yml under templates folder
- minio-route.yml under templates folder
- minio-route-https.yml under templates folder

## Assumptions and special notes

* This role can run only after you can login to your cluster by htpasswd user
* This role should run after playbook that will define what Storage class is default (Trident, for example)
* This role will always deploy minio route with https. If there are no custom certificates it will use default ingress certificates

## Common error
* In case that minio-tenant pods are stucked in crashLoopBack and the logs are about write permissions, consider adding 'securityContext: fsGroup: 999' to minio-tenant.yml and run again

## add to you inventory the following parameters:
--------------------------------------------------------------------------------
* **#Minio deployment**
* **minioproject:** quay-minio
* **miniohostname:** minio
* **quaybucketname:** quay-enterprise
* **minioaccesskey:** minio
* **miniosecretkey:** minio123
* **miniooperatorimage:** minio/k8s-operator:v3.0.1
* **minioimage:** minio/minio:latest
* **openshift_https_required:** false

When value of **openshift_https_required:** will be changed to "true" additional 3 parameters should be added to inventory

* **miniocertificate:** your certificate bundle
* **miniokey:** you key
* **miniocacertificate:** your ca certificate
